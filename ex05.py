#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

def main():
    a = []
    for x in range(10):
        a.append(random.randint(1, 10))

    b = []
    for x in range(12):
        b.append(random.randint(1, 10))

    # First method - complex constructs.
    c = []

    tmp = a if len(a) < len(b) else b
    for i in tmp:
        if i in b and i not in c:
            c.append(i)

    # Second method - one-liner.
    d = list(set(a) & set(b))

    print('a:', a)
    print('b:', b)
    print('c:', c)
    print('d:', d)


if __name__ == '__main__':
    main()
