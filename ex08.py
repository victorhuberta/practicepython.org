#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import random

def gpmove(moves) -> 'pmove or None':
    """
    Get player's move.

    Return None if it is an invalid move.
    """
    pmove = input('Your move: ')
    if pmove in moves: return pmove
    else: return None


def gaimove(moves) -> 'aimove':
    """
    Get AI's move.

    Unlike gpmove(), it always returns a move.
    """
    return random.choice(moves)


def rpsfight(rules, players_moves) -> 'winner or None':
    """
    Find the winner of a rock paper scissors (RPS) fight.

    Supported number of players is 2.
    Return None on draw or unsupported number of players is given.
    """
    n_supported_players = 2
    players = list(players_moves.keys())
    if len(players) != n_supported_players: return None

    p1 = players[0]
    mv1 = players_moves[p1]
    p2 = players[1] 
    mv2 = players_moves[p2]

    if rules[mv1] == mv2: return p1
    elif rules[mv2] == mv1: return p2
    else: return None


def main(*args, **kwargs):
    n_fights = 3
    moves = ['rock', 'paper', 'scissors']
    rules = {
        'rock': 'scissors',
        'paper': 'rock',
        'scissors': 'paper'
    }

    winboard = []
    pname = kwargs['pname'].capitalize()
    ainame = 'AI'

    for i in range(n_fights):
        while True:
            pmove = gpmove(moves)
            if pmove != None: break
            print('Invalid move. Get your shit together,', pname)
        aimove = gaimove(moves)
        winner = rpsfight(rules, {pname: pmove, ainame: aimove})
        if winner != None:
            winboard.append(winner)
            print(winner, 'wins this fight.')
        else:
            print('This fight is a draw.')

    pwinc = winboard.count(pname)
    aiwinc = winboard.count(ainame)
    winner = None

    if pwinc > aiwinc: winner = pname
    elif pwinc < aiwinc: winner = ainame

    if winner == None:
        print('It\'s a draw!')
    else:
        print(winner, 'wins the game.')


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: {prog} player'.format(prog=sys.argv[0]))
        sys.exit(1)

    main(pname=sys.argv[1])
    sys.exit(0)
