#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

class InvalidBoardError(Exception):
    pass


class ThisPlayerWins(Exception):
    pass


class EveryoneLoses(Exception):
    pass


class TTTBoard(object):
    """Represent a 3x3 Tic Tac Toe board."""
    
    def __init__(self, pname1, pname2):
        self.board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        self.players = {pname1: 1, pname2: 2}
    

    def update_board(self, pname, coordinate):
        self.board[coordinate[0]][coordinate[1]] = self.players[pname]
        res = self.check_game_result(self.board)
        if res == self.players[pname]:
            raise ThisPlayerWins
        elif res == None:
            if self.is_board_full(self.board):
                raise EveryoneLoses


    def check_game_result(self, board):
        try:
            int(board[0][0])
        except (TypeError, ValueError) as err:
            raise InvalidBoardError('The board has an invalid structure')
        else:
            # I'll come back once I found a better solution.
            if board[0][0] == board[0][1] == board[0][2] or \
                board[0][0] == board[1][0] == board[2][0] or \
                board[0][0] == board[1][1] == board[2][2]:
                    return board[0][0] if board[0][0] != 0 else None

            if board[0][1] == board[1][1] == board[2][1] or \
                board[0][2] == board[1][1] == board[2][0] or \
                board[1][0] == board[1][1] == board[1][2]:
                    return board[1][1] if board[1][1] != 0 else None

            if board[0][2] == board[1][2] == board[2][2] or \
                board[2][0] == board[2][1] == board[2][2]:
                    return board[2][2] if board[2][2] != 0 else None

        return None


    def is_board_full(self, board):
        total_count = 0
        for yboard in board:
            for xboard in yboard:
                if xboard != 0: total_count += 1

        return total_count == (len(board) * len(board[0]))


    def draw_horizontal(self, hsym='-', h=10, symcount=3):
        for _ in range(h):
            print(' ' + (symcount * hsym), end='')

        print()


    def draw_board(self, hsym='-', vsym='|', board=[[], [], []], h=10, v=10):
        for i in range(v):
            self.draw_horizontal(hsym=hsym, h=h, symcount=3)
            for j in range(h):
                if board[i][j] == 1: sym = 'X'
                elif board[i][j] == 2: sym = 'O'
                else: sym = ' '

                print('|' + (' ' + sym + ' ' if j != h else ''), end='')
                if j == h - 1:
                    print('|')

        self.draw_horizontal(hsym=hsym, h=h, symcount=3)


    def draw_tttboard(self):
        self.draw_board(board=self.board, h=3, v=3)



def main(*args, **kwargs):
    prog = kwargs['prog']
    pname1 = kwargs['pname1']
    pname2 = kwargs['pname2']
    pnames = [pname1, pname2]

    print('Welcome to Tic Tac Toe Combat!')

    ttt = TTTBoard(pname1, pname2)
    game_over = False
    
    while not game_over:
        for pname in pnames:
            ttt.draw_tttboard()

            print('{}\'s turn.'.format(pname))
            position = input('Provide the position e.g., x,y: ')

            try:
                xy = [int(c.strip()) for c in position.split(',')]
            except ValueError as err:
                print(pname, 'wasted their turn!')
                continue

            coordinate = (xy[1] - 1, xy[0] - 1) # x & y start from 1.
            try:
                ttt.update_board(pname, coordinate)
            except ThisPlayerWins:
                ttt.draw_tttboard()
                print(pname, 'has won the game!')
                game_over = True
                break
            except EveryoneLoses:
                ttt.draw_tttboard()
                print('The game is a DRAW!')
                game_over = True
                break
            except IndexError:
                print(pname, 'can\'t even count!')
                continue


if __name__ == '__main__':
    prog = sys.argv[0]

    if len(sys.argv) != 3:
        print('Usage: {} player1 player2'.format(prog))
        sys.exit(1)

    pname1 = sys.argv[1]
    pname2 = sys.argv[2]

    main(prog=prog, pname1=pname1, pname2=pname2)
