#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def draw_horizontal(hsym='-', h=10, symcount=3):
    for _ in range(h):
        print(' ' + (symcount * hsym), end='')

    print()


def draw_board(hsym='-', vsym='|', h=10, v=10):
    for _ in range(v):
        draw_horizontal(hsym=hsym, h=h, symcount=3)
        for i in range(h + 1):
            print('|' + ((3 * ' ') if i != h else ''), end='')
        print()

    draw_horizontal(hsym=hsym, h=h, symcount=3)


def main(*args, **kwargs):
    prog = kwargs['prog']
    width = kwargs['width']
    height = kwargs['height']

    draw_board(hsym='-', vsym='|', h=width, v=height)


if __name__ == '__main__':
    prog = sys.argv[0]
    
    if len(sys.argv) != 3:
        print('Usage: {} width height'.format(prog))
        sys.exit(1)

    try:
        width = int(sys.argv[1])
        height = int(sys.argv[2])
    except ValueError as err:
        print('{}: {}: {}'.format(prog, type(err).__name__, err))

    main(prog=prog, width=width, height=height)
