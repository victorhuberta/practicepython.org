#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def is_prime(num) -> bool:
    if num < 2: return False

    for i in range(2, num):
        if num % i == 0: return False

    return True


def main(*args, **kwargs):
    num = kwargs['num']
    if num < 0:
        print('Negative number. Try again!')
        sys.exit(1)

    if is_prime(num):
        print(num, 'is a prime number.')
    else:
        print(num, 'is NOT a prime number.')


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: {prog} number'.format(prog=sys.argv[0]))
        sys.exit(1)

    main(num=int(sys.argv[1]))
