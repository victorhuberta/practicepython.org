#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def main(*args, **kwargs):
    a = args[0]
    even = [x for x in a if x % 2 == 0]
    print(even)


if __name__ == '__main__':
    main([1, 4, 9, 16, 25, 36, 49, 64, 81, 100])
