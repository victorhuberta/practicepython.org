#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class InvalidBoardError(Exception):
    pass


def check_game_result(board):
    try:
        int(board[0][0])
    except (TypeError, ValueError) as err:
        raise InvalidBoardError
    else:
        # I'll come back once I found a better solution.
        if board[0][0] == board[0][1] == board[0][2] or \
            board[0][0] == board[1][0] == board[2][0] or \
            board[0][0] == board[1][1] == board[2][2]:
                return board[0][0] if board[0][0] != 0 else None

        if board[0][1] == board[1][1] == board[2][1] or \
            board[1][0] == board[1][1] == board[0][2]:
                return board[1][1] if board[1][1] != 0 else None

        if board[0][2] == board[1][2] == board[2][2] or \
            board[2][0] == board[2][1] == board[2][2]:
                return board[2][2] if board[2][2] != 0 else None

    return None
