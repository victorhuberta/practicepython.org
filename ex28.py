#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def max(*numbers):
    maxnum = numbers[0]

    for num in numbers[1:]:
        maxnum = num if num > maxnum else maxnum

    return maxnum


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Usage: {} a b c'.format(sys.argv[0]))
        sys.exit(1)

    numbers = [int(arg) for arg in sys.argv[1:]]

    print(max(*numbers))
