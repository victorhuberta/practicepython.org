#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def main():
    a = [13, 55, 89, 8, 5, 1, 1, 2, 34, 3, 21]
    a.sort()

    # First part.
    for i in a:
        if i < 5: print(i, end=' ')
    print()

    # Second part.
    na = []
    for i in a:
        if i < 5: na.append(i)
    print(na)

    # Third part.
    na = [i for i in a if i < 5]
    print(na)

    # Fourth part.
    lim = int(input('Give me the limit: '))
    na = [i for i in a if i < lim]
    print(na)


if __name__ == '__main__':
    main()
