#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def main():
    num = int(input('Give me a number: '))

    if num % 4 == 0:
        print(num, 'is a multiple of 4.')
    else:
        if num % 2 == 0:
            print(num, 'is an even number.')
        else:
            print(num, 'is an odd number.')

    while True:
        divisor = int(input('Give me a divisor: '))
        if divisor != 0: break
        else: print('0 is not allowed, bitch!')

    if num % divisor == 0:
        print(num, 'is divisible by', divisor)
    else:
        print(num, 'isn\'t divisible by', divisor)


if __name__ == '__main__':
    main()
