#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

def rmdup1(a):
    """
    Remove all duplicates from a list and return it.

    Use a loop for its implementation.
    """
    b = []
    for x in a:
        if x not in b: b.append(x)
    return b


def rmdup2(a):
    """
    Remove all duplicates from a list and return it.

    Use a set for its implementation.
    """
    return list(set(a))


def main():
    a = [random.randint(1, 10) for i in range(12)]
    b = rmdup1(a)
    c = rmdup2(a)

    print('a:', a)
    print('b:', b)
    print('c:', c)


if __name__ == '__main__':
    main()
