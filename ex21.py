#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import requests
from bs4 import BeautifulSoup

def find_heading(article):
    try:
        h2 = article.find('h2', {'class': 'story-heading'})
        heading = h2.find('a').text
    except AttributeError: return None
    else: return heading.strip() if len(heading) > 0 else None


def find_author(article):
    try:
        author = article.find('p', {'class': 'byline'}).text
    except AttributeError: return None
    else: return author.strip() if len(author) > 0 else None


def find_summary(article):
    try:
        summary = article.find('p', {'class': 'summary'}).text
    except AttributeError: return None
    else: return summary.strip() if len(summary) > 0 else None


def main(*args, **kwargs):
    prog = kwargs['prog']
    ofpath = kwargs['ofpath']
    try:
        outfile = open(ofpath, 'w')
    except IOError as err:
        print('{}: {}: {}'.format(prog, type(err).__name__, err))
        sys.exit(1)

    nytimes = requests.get('http://www.nytimes.com')
    doc = BeautifulSoup(nytimes.text)
    articles = doc.find_all('article', {'class': 'story theme-summary'})

    for article in articles:
        heading = find_heading(article)
        author = find_author(article)
        summary = find_summary(article)

        if None in (heading, author, summary): continue

        outfile.write((80 * '=') + '\n')
        outfile.write('Heading: {}\n'.format(heading))
        outfile.write('Author: {}\n'.format(author))
        outfile.write('Summary: {}\n'.format(summary))
        outfile.write((80 * '=') + '\n')

    outfile.close()

    if len(articles) == 0: print('No articles for today.')


if __name__ == '__main__':
    prog = sys.argv[0]

    if len(sys.argv) != 2:
        print('Usage: {} output-file'.format(prog))
        sys.exit(1)

    ofpath = sys.argv[1]

    main(prog=prog, ofpath=ofpath)
