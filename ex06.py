#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def is_palindrome(s1):
    s2 = ''.join([c for c in reversed(s1)])

    return s1 == s2


def main(*args, **kwargs):
    s = kwargs['s']

    fmtargs = {'string': s, 'not': '' if is_palindrome(s) else ' not'}
    print('{string} is{not} a palindrome.'.format(**fmtargs))


if __name__ == '__main__':
    if len(sys.argv) != 2 or sys.argv[1] == '--help':
        print('Usage: {prog} string'.format(prog=sys.argv[0]))
        sys.exit(1)

    main(s=sys.argv[1])
    sys.exit(0)
