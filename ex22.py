#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def main(*args, **kwargs):
    prog = kwargs['prog']
    fname = kwargs['fname']
    categories = {}

    with open(fname, 'r') as f:
        for line in f:
            category = '/'.join(line.split('/')[1:-1])
            categories[category] = 1 \
                if category not in categories else categories[category] + 1

    for category, count in categories.items():
        print("{}: {}".format(category, count))


if __name__ == '__main__':
    prog = sys.argv[0]

    if len(sys.argv) != 2:
        print("Usage: {} filename".format(prog))
        sys.exit(1)

    fname = sys.argv[1]

    main(prog=prog, fname=fname)
