#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def main(*args, **kwargs):
    sentence = kwargs['sentence']
    words = sentence.split(' ')
    rwords = list(reversed(words))
    rsentence = ' '.join(rwords)
    print(rsentence)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: {} words...'.format(sys.argv[0]))
        sys.exit(1)

    sentence = ' '.join(sys.argv[1:])
    main(sentence=sentence)
