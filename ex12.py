#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def head(elements, n=1):
    """ Take N elements from beginning of a list. """ 
    return elements[:n]


def tail(elements, n=1):
    """ Take N elements from end of a list. """ 
    return elements[len(elements) - n:]


def main(*args, **kwargs):
    elements = kwargs['elements']
    new_elems = head(elements) + tail(elements)

    print('elements:', elements)
    print('new elements:', new_elems)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: {prog} elements...'.format(prog=sys.argv[0]))
        sys.exit(1)

    elements = sys.argv[1:]

    main(elements=elements)
