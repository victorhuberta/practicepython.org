#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import random
import string

def gendigits(length):
    for _ in range(length):
        yield random.choice(string.digits)


def get_cows_and_bulls(num, guess):
    cows, bulls = 0, 0

    for a, b in zip(num, guess):
        if a == b: cows += 1
        elif a != b and (b in num): bulls += 1

    return (cows, bulls)


def print_err(prog, err):
    fmtargs = {
        'prog': prog,
        'errname': type(err).__name__,
        'errmsg': err
    }
    print('{prog}: {errname}: {errmsg}'.format(**fmtargs))


def main(*args, **kwargs):
    prog = kwargs['prog']
    num = ''.join([c for c in gendigits(4)])

    print('Welcome to the Cows and Bulls Game!')
    print('Enter a number:')

    n_guesses = 0
    while True:
        try:
            guess = input('>>> ')
        except EOFError as err:
            print_err(prog, err)
            sys.exit(0)

        cows, bulls = get_cows_and_bulls(num, guess)
        print('{} cow(s), {} bull(s)'.format(cows, bulls))
        
        n_guesses += 1
        if cows == 4: break

    print('Total guesses:', n_guesses)


if __name__ == '__main__':
    main(prog=sys.argv[0])
