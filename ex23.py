#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def print_err(prog, err):
    print('{}: {}: {}'.format(prog, type(err).__name__, err))
    sys.exit(1)


def get_numbers(fname):
    with open(fname, 'r') as f:
        try:
            l = [int(line) for line in f]
        except ValueError as err:
            print_err(prog, err)

    return l


def main(*args, **kwargs):
    prog = kwargs['prog']
    first = kwargs['first']
    second = kwargs['second']
    
    primes = set(get_numbers(first))
    happies = set(get_numbers(second))
    overlapped = primes & happies
    print(overlapped)


if __name__ == '__main__':
    prog = sys.argv[0]

    if len(sys.argv) != 3:
        print('Usage: {} first second'.format(prog))
        sys.exit(1)

    first, second = sys.argv[1], sys.argv[2]

    main(prog=prog, first=first, second=second)
