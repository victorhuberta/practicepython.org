#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

def main(*args, **kwargs):
    a = [random.randint(1, 10) for i in range(20)]
    b = [random.randint(1, 10) for i in range(10)]

    tmp = a if len(a) < len(b) else b
    c = [x for x in a if x in b]
    c = list(set(c)) # Remove duplicates.

    print('a:', a)
    print('b:', b)
    print('c:', c)


if __name__ == '__main__':
    main()
