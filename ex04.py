#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def main():
    num = int(input('Give me the number you want me to find the divisor of: '))
    res = list(filter(lambda x: num % x == 0, range(1, num + 1)))
    print(res)


if __name__ == '__main__':
    main()
