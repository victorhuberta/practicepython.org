#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import random
import string

class PasswordComplexityError(Exception):
    pass


class RandomGenerator():
    """A generator for random sequences.

    Current supported random sequences are numbers, alphabets,
    ASCII symbols, all can be mixed in uppercase or lowercase.
    """

    def __init__(self, seed=None):
        # Seed the underlying module `random`.
        self.seed = seed
        random.seed(a=self.seed, version=2)

        self.tcharset = ''
        self.is_tcharset_changed = False

        # Specify default requirements.
        self.specify()


    def specify(self, alpha=1, num=0, sym=0, upper=0, lower=1):
        """Specify the requirements of the randomly generated
        sequence of characters.

        Default arguments:
            alpha -- sequence contains alphabets
            num -- sequence contains numbers
            sym -- sequence contains ASCII symbols
            upper -- sequence contains uppercase letters
            lower -- sequence contains lowercase letters
        """

        self.alpha = alpha
        self.num = num
        self.sym = sym
        self.upper = upper
        self.lower = lower
        self.is_tcharset_changed = True


    def grndchars(self, length):
        """Generate a random sequence of characters based on the
        given specification.
        """

        if self.is_tcharset_changed:
            self.is_tcharset_changed = False
            self.tcharset = ''

            if self.alpha:
                if self.lower: self.tcharset += string.ascii_lowercase
                if self.upper: self.tcharset += string.ascii_uppercase
            if self.num: self.tcharset += string.digits
            if self.sym: self.tcharset += string.punctuation

        return (random.choice(self.tcharset) for _ in range(length))


    def grndpass(self, plen, complexity=1):
        """Generate a random password based on the given
        password's complexity.

        Arguments:
            plen -- length of generated password
            complexity -- the level of password's complexity
                It is measured in 0 - 4 in increasing complexity.
        """

        if complexity == 0:
            # TODO: Take a word from a dictionary.
            pass
        elif complexity == 1:
            self.specify(alpha=1, lower=1)
        elif complexity == 2:
            self.specify(alpha=1, lower=1, num=1)
        elif complexity == 3:
            self.specify(alpha=1, lower=1, num=1, upper=1)
        elif complexity == 4:
            self.specify(alpha=1, lower=1, num=1, upper=1, sym=1)
        else:
            raise PasswordComplexityError('invalid complexity level')

        return ''.join([c for c in self.grndchars(plen)])


def main(*args, **kwargs):
    plen = kwargs['plen']
    complexity = kwargs['complexity']
    rg = RandomGenerator()
    try:
        passwd = rg.grndpass(plen, complexity)
        print(passwd)
    except PasswordComplexityError as err:
        print('{prog}: {name}: {msg}'.format
            (prog=prog, name=type(err).__name__, msg=err))


if __name__ == '__main__':
    prog = sys.argv[0]

    if len(sys.argv) != 3:
        print('Usage: {} pass-len complexity'.format(prog))
        print('    complexity -- password\'s complexity level [0 - 4]')
        sys.exit(1)

    try:
        plen = int(sys.argv[1])
        complexity = int(sys.argv[2])
    except ValueError as err:
        print('{}: invalid type of argument(s) given.'.format(prog))
        sys.exit(1)
    else:
        main(prog=prog, plen=plen, complexity=complexity)
