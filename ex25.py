#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def find_number(numbers):
    top = len(numbers)
    mid = top // 2
    bottom = 0
    n_guesses = 0

    while True:
        guess = numbers[mid]
        print('Is your number... {}?'.format(guess))

        try:
            feedback = int(input
                ('(1) too low -- (2) too high -- (3) just right!: '))
        except ValueError as err:
            print('{}: {}'.format(type(err).__name__, err))
            sys.exit(1)
        else:
            if feedback == 1:
                bottom = mid
                mid += (top - bottom) // 2
            elif feedback == 2:
                top = mid
                mid = bottom + (top - bottom) // 2
            elif feedback == 3:
                n_guesses += 1
                break
            else:
                continue

        n_guesses += 1

    return n_guesses


def main(*args, **kwargs):
    prog = kwargs['prog']

    name = input('Hi, what is your name? ')
    print(name, ', think of a number between 0 - 100.')
    print('I\'ll try to guess your number!')
    input('Press ENTER when you are ready :)')
    print()

    numbers = [i for i in range(101)]
    n_guesses = find_number(numbers)
    print()
    print('I guessed {} time(s)!'.format(n_guesses))


if __name__ == '__main__':
    main(prog=sys.argv[0])
