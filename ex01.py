#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import date

def main():
    name = input('What is your name? ')
    age = int(input('Please enter your age: '))

    year100 = 100 - age + date.today().year
    msg = name + ', you will turn 100 years old in ' + str(year100)

    times = int(input('How many times do I have to tell you? '))
    for i in range(times):
        print(msg)

if __name__ == '__main__':
    main()
