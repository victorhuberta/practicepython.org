#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import random

def main(*args, **kwargs):
    ans = ''
    
    while True:
        print('\nComing up with a random number...\n')
        n_guesses = 0
        num = random.randint(1, 9)

        while True:
            ans = input('Guess the number ("exit" or "quit" to stop): ')
            if ans in ('exit', 'quit'): sys.exit(0)

            if ans.isnumeric():
                guess = int(ans)
            else:
                print('...not a number, moron.')
                continue

            n_guesses += 1

            if guess < num:
                print('Too low!')
            elif guess > num:
                print('Too high!')
            else:
                print('Exactly right ;)')
                print('Number of guesses: ', n_guesses)
                break


if __name__ == '__main__':
    main()
