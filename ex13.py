#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def gen_fibo(fibonum=10):
    fibo = []
    a, b = 0, 1

    for i in range(fibonum):
        fibo.append(a)
        a, b = b, a + b 

    return fibo


def main(*args, **kwargs):
    fibonum = kwargs['fibonum']
    fibo = gen_fibo(fibonum)
    print(fibo)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: {} number_of_fibo'.format(sys.argv[0]))
        sys.exit(1)

    main(fibonum=int(sys.argv[1]))
