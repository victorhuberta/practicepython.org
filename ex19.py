#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import requests
from bs4 import BeautifulSoup

def find_title(doc):
    try:
        title = doc.find('h1', {'class': 'hed'}).text
    except AttributeError: return None
    else: return title if len(title) > 0 else None


def find_intro(doc):
    try:
        intro = doc.find('div', {'class': 'dek'}).text
    except AttributeError: return None
    else: return intro if len(intro) > 0 else None


def find_sections(doc):
    return doc.find_all('section',
        {'class': 'content-section', 'data-type': 'section'})


def find_paragraphs(sections):
    for section in sections:
        for paragraph in section.find_all('p'):
            yield paragraph.text


def main(*args, **kwargs):
    vanityfair = requests.get('http://www.vanityfair.com/style/'
        'society/2014/06/monica-lewinsky-humiliation-culture')
    doc = BeautifulSoup(vanityfair.text)
    
    title = find_title(doc)
    intro = find_intro(doc)
    sections = find_sections(doc)

    print('Title:', title)
    print()
    print(intro)
    print()
    for paragraph in find_paragraphs(sections):
        print()
        print(paragraph)


if __name__ == '__main__':
    prog = sys.argv[0]

    main(prog=prog)
