#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def find_elem(ordlist, elem):
    return elem in ordlist


def is_ordered(alist):
    for i in range(len(alist)):
        if i == (len(alist) - 1): return True
        if alist[i] > alist[i + 1]: return False

    return True


def binsearch(alist, elem):
    length = len(alist)

    mid = length // 2
    if mid == 0:
        if length == 0: return False
        else: return elem == alist[0] # base case

    if elem == alist[mid]: return True
    elif elem < alist[mid]: return binsearch(alist[:mid], elem)
    else: return binsearch(alist[mid + 1:], elem)


def bfind_elem(alist, elem):
    if not is_ordered(alist):
        alist.sort()

    return binsearch(alist, elem)
